package ProjectA;

import java.util.ArrayList;
/**
 * Player
 */
public class Player extends Entity {

    public boolean isBought;
    private ArrayList<String> playerSkills;
    protected ArrayList<ItemWeapon> playerWeapons;

    public Player(){
        super();
        this.maxHealth = 100;
        this.currentHealth = this.maxHealth;
        this.playerSkills = new ArrayList<String>();
        this.playerWeapons = new ArrayList<ItemWeapon>();
        this.mapRepresentation = "P";
    }
    
    public void itemBuy(Item item, Village village){
        if(this.currency - item.cost >= 0) {
            if(item.code < 50)
                this.inventory.add(item);
            else
                this.playerWeapons.add((ItemWeapon)item);
            village.inventory.remove(item);
            this.currency -= item.getCost();
        }
        else this.isBought = true;
        
    }

    public void itemDrop(Item item){
        this.inventory.remove(item);
    }

    public void itemUse(Item item){

    }
    
    public void talk(Entity ent){
        
    }

    public void learnSkill(String skill){
        this.playerSkills.add(skill);
    }

    public void skillTest(){
        if (playerSkills.contains("skillTest"))System.out.println(01);
    }

    public String toString(){
        return String.format("Gold: %d\nDamage: %.0f\nEquipped Weapon: %s", 
        this.currency,
        this.attackDamage,
        this.equippedWeapon.itemName);
    }
}
