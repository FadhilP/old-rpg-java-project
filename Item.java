package ProjectA;

import java.util.Random;
/**
 * Items
 */
public class Item {

    protected String itemName;
    protected String itemDescription;
    
    protected int code;
    protected int cost;

    protected double durability;
    protected double initialDura;

    protected static Item[] itemArchive = {
        new Item("Potion", "Heals you for 15HP", 10, 3, 0)
    };


    public Item(String itemName, String itemDescription, int cost, int durability, int code){
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.cost = cost;
        this.durability = durability;
        this.initialDura = durability;
        this.code = code;
    }

    public Item(){
        int randItem = new Random().nextInt(itemArchive.length);
        this.itemName = itemArchive[randItem].itemName;
        this.itemDescription = itemArchive[randItem].itemDescription;
        this.cost = itemArchive[randItem].cost;
        this.durability = itemArchive[randItem].durability;
        this.code = itemArchive[randItem].code;
    }

    public static Item randomItem(){
        int randPick = new Random().nextInt(2);
        if(randPick == 0) return new Item();
        return new ItemWeapon();
    }

    public int getCost(){
        return this.cost;
    }

    public String toString(){
        return String.format("%s\nCost: %dG\nUses: %.0f\nDesc: %s", 
        this.itemName, 
        this.cost,
        this.durability,
        this.itemDescription);
    }
    
}