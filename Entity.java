package ProjectA;

import java.util.ArrayList;
import java.util.Random;
/**
 * Entity
 */
public class Entity {

    protected ArrayList<Item> inventory;
    protected Random rand;

    protected String name;
    public String mapRepresentation;

    protected double attackDamage;
    protected double currentHealth;
    protected double maxHealth;
    protected double mana;
    
    protected int currency;
    protected int xp;
    protected int x;
    protected int y;

    protected ItemWeapon equippedWeapon;

    public Entity(){
        this.rand = new Random();
        this.equippedWeapon = new ItemWeapon(false);
        this.attackDamage = 1 * equippedWeapon.getDamage();
        this.inventory = new ArrayList<Item>();
        this.maxHealth = 100;
        this.currentHealth = maxHealth;
        this.mana = 0;
        this.currency = 10;
    }

    public Entity(boolean isEntity){
        this.mapRepresentation = "-";
    }

    public void attack(Entity other){
        other.currentHealth -= this.attackDamage;
    }

    public void heal(){
        if(this.currentHealth <= this.maxHealth * 0.98)
        this.currentHealth += this.maxHealth * 0.02;
    }
}