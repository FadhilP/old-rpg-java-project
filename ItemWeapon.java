package ProjectA;

import java.util.Random;
/**
 * ItemWeapon
 */
public class ItemWeapon extends Item {

    private double damage;
    protected static ItemWeapon[] itemWeaponArchive ={
        new ItemWeapon("Wooden Sword", "Basic Sword", 5, 100, 15, 50)
    };

    public ItemWeapon(){
        int randItem = new Random().nextInt(itemArchive.length);
        this.itemName = itemWeaponArchive[randItem].itemName;
        this.itemDescription = itemWeaponArchive[randItem].itemDescription;
        this.damage = itemWeaponArchive[randItem].damage;
        this.cost = itemWeaponArchive[randItem].cost;
        this.durability = itemWeaponArchive[randItem].durability;
        this.code = itemWeaponArchive[randItem].code;
    }

    public ItemWeapon(boolean x){
        this.itemName = "None";
        this.damage = 10;
        this.durability = Double.POSITIVE_INFINITY;
    }

    public ItemWeapon(String itemName, String itemDescription, int cost, int durability, double damage, int code){
        super(itemName, itemDescription, cost, durability, code);
        this.damage = damage;
        
    }

    public double getDamage(){
        return this.damage;
    }

    public String toString(){
        return String.format("%s\nCost: %dG\nDurability: %.0f\nDamage: %.0f\nDesc: %s", 
        this.itemName, 
        this.cost,
        this.durability,
        this.damage,
        this.itemDescription);
    }
}