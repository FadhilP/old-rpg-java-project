package ProjectA;

import java.util.ArrayList;

/**
 * Village
 */
public class Village extends Entity{

    private ArrayList<Entity> inhabitant; 

    private static String[] villageNameArchive ={
        "Acanon",
        "Accatran",
        "Albia",
        "Anvilus",
        "Arcadia", 
        "Acheron", 
        "Armageddon", 
        "Avallain", 
        "Abdiel",
        "Adnachiel", 
        "Adramelechk", 
        "Afriel",
        "Ambriel", 
        "Amitiel", 
        "Anael", 
        "Anahita",
        "Anauel",
        "Ananchel",
        "Appoloin",
        "Ariel",
        "Armaita",
        "Asmodel",
        "Albus",
        "Aurantiacus",
        "Asur",
        "Aliatoc",
        "Altansar",
        "Baal",
        "Balur",
        "Balthon",
        "Barbarus",
        "Belial",
        "Begnion",
        "Brass Keep",
        "Breder",
        "Bohsenfels",
        "Boven",
        "Baglis",
        "Balthial",
        "Barakiel",
        "Barbelo",
        "Barbiel",
        "Barchiel",
        "Bath Kol",
        "Biel-Tan",
        "Cadia",
        "Catachan",
        "Caliban",
        "Calth",
        "Chemos",
        "Circe",
        "Colchis",
        "Corinth",
        "Charadon",
        "Cthonia",
        "Cypra Mundi",
        "Corax",
        "Camael",
        "Cassiel",
        "Cathetel",
        "Chamuel",
        "Charmeine",
        "Charoum",
        "Cherubim",
        "Colopatiron",
        "Carnei",
        "Commorragh",
        "Croesus",
        "Ctho",
        "Coritanorum",
        "Caledonian"
    };

    public Village(){
        super();
        this.mapRepresentation = "V";
        inhabitant = new ArrayList<Entity>();
        this.maxHealth = 1000;
        this.currentHealth = this.maxHealth;
        this.name = villageNameArchive[rand.nextInt(villageNameArchive.length)];
        this.generateItem(10);
    }
        
    public void printPopulation(){
        System.out.print(this.name + " : ");
        for (Entity x : this.inhabitant){
            System.out.print(x + ", ");
        }
        System.out.println();
    }

    public void autoGenerateInhabitants(){
        int bound = rand.nextInt(10) + 10;
        for (int i = 0; i < bound; i++){
            inhabitant.add(new Human());
        }
    }

    public void generateItem(int count){
        for(int i = 0; i < count; i++){
            this.inventory.add(Item.randomItem());
        }
    }

    public void printItem(){
        int counter = 1;
        for (Item item : this.inventory){
            System.out.printf("%d. %4s\n", counter, item.itemName);
            counter++;
        }
    }
    
    public String toString(){
        return String.format("Welcome to %s village!", this.name);
    }

}