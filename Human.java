package ProjectA;

import java.util.Random;

/**
 * Human
 */
public class Human extends Entity {

    private int age;

    private static String[] humanNameArchive = 
    {"Jack", "John", "Susan", "Rose", "Dimas", "Sarah", "Joe", "Will",
     "Matt", "Lance", "Natalie", "Anna"};

    public Human(String name){
        super();
        this.age = new Random().nextInt(70);
    }

    public Human(String name, int age){
        this(name);
        this.age = age;
    }

    public Human(){
        this.name = humanNameArchive[new Random(). nextInt(3)];
        this.age = new Random().nextInt(70);
    }

    public String getCategory(){
        if (this.age < 11) return "Child";
        if (this.age < 19) return "Teen";
        return "Adult";
    }
    
    public String toString(){
        return String.format("{%s : %d}", this.name, this.age);
    }
}