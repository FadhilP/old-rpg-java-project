package ProjectA;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;
/**
 * Main
 */
public class ProjectMain {
    public static void main(String[] args){;
        
        String version = "Alpha 0.13";

        //TODO : Abilities Interface.java
        
        Random rand = new Random();

        //Stored Monsters that exist in the generated world
        ArrayList<Monster> monsterData = new ArrayList<Monster>();

        //Monster that are visible on your map
        ArrayList<Monster> monsterMapData = new ArrayList<Monster>();

        //Monster that are in range of your attack
        ArrayList<Monster> monsterInRange = new ArrayList<Monster>();

        //Stored Village that exist in the generated world
        ArrayList<Village> villageData = new ArrayList<Village>();

        //Check near village that are within of the player's reach
        Village villageInRange = new Village();

        //Initiate Scanner
        Scanner input = new Scanner(System.in);
        String playerInput = null;

        //Initiate a false entity and wall entity
        Entity notEnt = new Entity(false);
        Entity wallEnt = new Entity(false); wallEnt.mapRepresentation = "# ";

        //Initate Targeted monster
        Monster monsterTarget;

        //Initate UI check
        boolean inspectCheck = false;
        boolean attackCheck = false;
        boolean inspectVillageCheck = false;
        boolean playerDetailsCheck = true;


        //Monster turn
        boolean monsterIsChase = true;
        boolean monsterAttackCheck = false;

        //Loop check when cls runs
        boolean loopInspect = false;
        boolean loopPlayerAttack = false;
        boolean loopMonsterAttack = false;
        boolean loopShowArrow = true;
        boolean loopVillage = false;
        boolean loopBuy = false;
        boolean loopBuyChoose = false;
        boolean loopPlayerDetails = false;
        boolean loopEquipWep = false;
        boolean loopChooseWep = false;

        String playerAttack = "-1";
        String playerVillage = "-1";
        String playerBuy = "-1";
        String playerChooseBuy = "-1";
        String playerDetails = "-1";
        String playerWep = "-1";
        String playerChooseWep = "-1";


        System.out.println("The Adventure of Cephas Knight\nVersion " + version);
        System.out.println("Press enter to continue...");
        input.nextLine();
        cls();

        Monster.gameStage = 1;

        villageData.add(new Village());
        villageData.get(0).x = 25;
        villageData.get(0).y = 25;

        Player player = new Player();
        player.x = 30;
        player.y = 20;

        //Map Size
        Entity[][] map = new Entity[211][211];

        //Fill the entire map with an empty Entity
        for (int i = 0; i < map.length; i++){
            for (int j = 0; j < map[i].length; j++){
                if (i == 6 || i == map.length-6 || j == 6 || j== map.length-6) map[i][j] = wallEnt;
                else map[i][j] = notEnt;
            }
        }
        while(true){

            System.out.println("The Adventure of Cephas Knight\nVersion " + version);

            inspectCheck = false;
            attackCheck = false;
            inspectVillageCheck = false;
            playerDetailsCheck = true;

            monsterAttackCheck = false;

            //Reset monster that are seen in the map
            monsterMapData.clear();

            //Reset monster that are in range of the player
            monsterInRange.clear();

            //Locate the player on the map
            map[player.y][player.x] = player;

            //Locate every monstser on the map
            for(Monster monster : monsterData){
                map[monster.y][monster.x] = monster;
            }

            //Locate every village on the map
            for(Village village : villageData){
                map[village.y][village.x] = village;
            }
            
            //Print the current X, Y position of the player
            System.out.printf("\n\n  Current Position : {%d, %d}\n", player.x, player.y);

            //Draw the map centered around the player
            for(int i = player.y-5; i < player.y+6; i++){
                System.out.println();
                System.out.print("    ");
                for(int j = player.x-5; j < player.x+6; j++){
                    //Map Representation is stored on each class
                    System.out.print(map[i][j].mapRepresentation + " ");

                    //Check if there is a monster on the map that the player can inspect
                    //If there is one, then it will add to the monsterLocation ArrayList
                    //In the next Loop it will check if that current entity has already been stored
                    if(map[i][j].mapRepresentation.equals("M")){
                        if(monsterMapData.contains(map[i][j])) continue;
                        monsterMapData.add((Monster)map[i][j]);
                        inspectCheck = true;
                    }
                    
                    //Check if there is a village on the map that the player can visit
                    //Player can only visit a village when the player is in a range of a block 
                    if(map[i][j].mapRepresentation.equals("V")){
                        for(Village village : villageData){
                            if(map[i][j] == village){
                                if(Math.abs(village.x - player.x) == 1 && village.y - player.y == 0 ||
                                   village.x - player.x == 0 && Math.abs(village.y - player.y) == 1){ 
                                    villageInRange = village;
                                    inspectVillageCheck = true;
                                } 
                            }
                        }
                    }
                }
            }
            
            
            //Check coordinates based on monsterLocation to know which monster are in range of the player for attacking
            //Monster that are in range of the player to attack is added to monsterInRange ArrayList
            for(Monster monster : monsterMapData){
                if(monster.currentHealth > 0){
                    if(Math.abs(monster.x - player.x) == 1 && monster.y - player.y == 0){ 
                        monsterInRange.add(monster);
                        attackCheck = true;
                        monsterAttackCheck = true;
                    } 

                    else if(monster.x - player.x == 0 && Math.abs(monster.y - player.y) == 1){ 
                        monsterInRange.add(monster);
                        attackCheck = true;
                        monsterAttackCheck = true;
                    }

                    //Monsters will heal if the player is not in range
                    else if (!loopInspect) monster.heal();

                    //Choose a random Monster movement
                    int monsterMoveCheck = rand.nextInt(4);
                    
                    //TODO : MONSTER CHASE AI
                    monsterIsChase = false;
                    //monsterChase is false when player choose to inspect the surrounding monsters
                    if(monsterIsChase){
                        if(monsterMoveCheck == 0){
                            //Monster move RIGHT
                            if(player.x > monster.x && map[monster.y][monster.x+1] == notEnt){ 
                                monster.x += 1;
                                map[monster.y][monster.x-1] =  notEnt;
                            }
                            //Monster move LEFT
                            else if(player.x < monster.x && map[monster.y][monster.x-1] == notEnt){
                                monster.x -= 1;
                                map[monster.y][monster.x+1] = notEnt;
                            }
                        }

                        else if(monsterMoveCheck == 1){
                            //Monster move DOWN
                            if (player.y > monster.y && map[monster.y+1][monster.x] == notEnt){ 
                                monster.y += 1;
                                map[monster.y-1][monster.x] = notEnt;
                            }
                            //Monster move UP
                            else if(player.y < monster.y && map[monster.y-1][monster.x] == notEnt){
                                monster.y -= 1;
                                map[monster.y+1][monster.x] = notEnt;
                            }
                        }
                    }
                    else monsterIsChase = true;
                }
            }

            System.out.printf("\n\n");

            //If there a no monsters near the player, the player will heal
            if(monsterInRange.size() == 0 && !loopInspect){
                player.heal();
               }

            //Monster attack the player
            //If the player inspect the monster will not make a move
            if(loopMonsterAttack && !loopInspect){
                for (Monster monster : monsterInRange){
                    monster.attack(player);
                    System.out.printf("      %s attacked you!\n", monster.type);
                }
                loopMonsterAttack = false;
            }

            //Print the player's current health
            System.out.printf("        Health: %.0f/%.0f\n\n", player.currentHealth, player.maxHealth);

            //If the player chooses to Inspect, loopInspectCheck will be true
            //Inspect does not use a player's turn
            //Inspecting a monster will print its name, health and coordinates
            if(loopInspect){
                System.out.println("=====================================");
                for (Monster monster : monsterMapData){
                    System.out.println(monster);
                    System.out.println("=====================================");
                }
                loopInspect = false;
                monsterIsChase = true;
            }

            if(player.isBought) {
                System.out.println("Not enough money!\n");
                player.isBought = false;
            }

            //Village Inspect
            //Doing activity in the village will not trigger monster movement
            if(loopVillage){
                if(playerVillage.equals("-1")){
                    System.out.println(villageInRange);
                    System.out.println("0. Back\n1. Buy");
                    playerVillage = input.nextLine();
                }

                //Back to previous screen
                if(playerVillage.equals("0")){
                    playerVillage = "-1";
                    loopVillage = false;
                    cls();
                    continue;
                }

                //If player chooses to buy the listed items, it will print the details of the item
                //If the player buy the chosen item, it will add it to the inventory, and remove it from the village inventory
                if(loopBuyChoose){
                    System.out.printf("Gold: %d\n\n", player.currency);
                    System.out.println(villageInRange.inventory.get(Integer.parseInt(playerBuy)-1));
                    System.out.println("\n1. Buy Item\n\n0. Back");

                    if(playerChooseBuy.equals("-1")) 
                        playerChooseBuy = input.nextLine();
                    if(playerChooseBuy.equals("1"))
                        player.itemBuy(villageInRange.inventory.get(Integer.parseInt(playerBuy)-1), villageInRange);

                    playerChooseBuy = "-1";
                    playerBuy = "-1";
                    loopBuyChoose = false;
                    cls();
                    continue;
                }

                //Print the village Inventory
                if(loopBuy){
                    System.out.printf("Gold: %d\n\n", player.currency);
                    villageInRange.printItem();
                    System.out.println("\n0. Back");
                    playerBuy = input.nextLine();
                    //Previous screen
                    if(playerBuy.equals("0")){
                        playerVillage = "-1";
                        playerBuy = "-1";
                        loopBuy = false;
                    }
                    else loopBuyChoose = true;
                    cls();
                    continue;
                }

                //If player chooses to buy items from the village
                //The Village will print its inventory
                if(playerVillage.equals("1")){
                    loopBuy = true;
                    cls();
                    continue;
                }
                loopVillage = false;
                monsterIsChase = true;

            }

            //If the player chooses to Attack, loopPlayerAttackCheck will be true
            if(loopPlayerAttack){
                //Check if there is more than 1 monster in range of the player
                if (monsterInRange.size() > 1){
                    System.out.println("Choose which monster to attack");
                    int counter = 1;
                    //Will print the name of the monsters that are in range
                    //Other attributes such as health can checked with Inspect Monster without using a turn
                    for (Monster monster : monsterInRange){
                        System.out.printf("%d. %s\n", counter, monster.type);
                        counter++;
                    }
                    System.out.println("0. Back");
                    //if playerAttack is not -1, that means it has looped over carrying the monster that the player want to attack
                    if(playerAttack.equals("-1")) {
                        playerAttack = input.nextLine();
                        cls();
                        continue;
                    }

                    //Back function if player the player decided to another command
                    if(playerAttack.equals("0")) {
                        loopPlayerAttack = false;
                        playerAttack = "-1";
                        monsterIsChase = false;
                        cls();
                        continue;
                    }

                    //Player input attack the monster
                    else{
                        player.attack(monsterInRange.get(Integer.parseInt(playerAttack)-1));
                        System.out.printf("%s took %.0f damage!\n", monsterInRange.get(Integer.parseInt(playerAttack)-1).type, player.attackDamage);
                        monsterTarget = monsterInRange.get(Integer.parseInt(playerAttack)-1);
                        playerAttack = "-1";
                    }
                }

                //If there is only 1 monster around the player
                else{
                    player.attack(monsterInRange.get(0));
                    System.out.printf("%s took %.0f damage!\n", monsterInRange.get(0).type, player.attackDamage);
                    monsterTarget = monsterInRange.get(0);
                }
                //If the monster has 0 health
                if(monsterTarget.currentHealth <= 0) {
                    monsterTarget.mapRepresentation = "-";
                    //Player's gold will be added
                    player.currency += monsterTarget.currency;
                    player.xp += monsterTarget.xp;
                    //Remove monster from data
                    monsterData.remove(monsterTarget);
                    attackCheck = false;
                    loopPlayerAttack = false;
                    monsterAttackCheck = false;
                    cls();
                    continue;
                    
                }
                loopPlayerAttack = false;
                System.out.println();
            }

            //Show the player the basic controls the first time they started;
            if(loopShowArrow){
                System.out.println("W: Forward\nA: Left\nS: Downn\nD: Right");
                loopShowArrow = false;
            }
            
            if(loopPlayerDetails){
                if(playerDetails.equals("-1")){
                    System.out.println(player);
                    System.out.println("\nE: Equip Weapon");
                    playerDetails = input.nextLine().toUpperCase();
                }

                if(playerDetails.equals("0")){
                    playerDetails = "-1";
                    loopPlayerDetails = false;
                    cls();
                    continue;
                }
                if(loopChooseWep){
                    System.out.println(player.playerWeapons.get(Integer.parseInt(playerWep)-1));
                    System.out.println("\n1. Equip\n\n0. Back");

                    if(playerChooseWep.equals("-1")) 
                        playerChooseWep = input.nextLine();
                    if(playerChooseWep.equals("1")){
                        player.equippedWeapon = player.playerWeapons.get(Integer.parseInt(playerWep)-1);
                        player.attackDamage = 1 * player.equippedWeapon.getDamage();
                    }

                    playerChooseWep = "-1";
                    playerWep = "-1";
                    loopChooseWep = false;
                    loopEquipWep = false;
                    cls();
                    continue;
                }

                if(loopEquipWep){
                    System.out.println("Equip a weapon:");
                    int counter = 1;
                    if(player.playerWeapons.size() != 0){
                        for(Item itemWep : player.playerWeapons){
                        System.out.printf("%d. %s\n", counter, itemWep.itemName);
                        counter++;
                        }
                    }
                    else System.out.println("You have no weapons");
                    System.out.println("\n0. Back");
                    playerWep = input.nextLine();

                    if(playerWep.equals("0")){
                        playerDetails = "-1";
                        playerWep = "-1";
                        loopEquipWep = false;
                    }
                    else loopChooseWep = true;
                    cls();
                    continue;
                }

                if(playerDetails.equals("E")){
                    loopEquipWep = true;
                    cls();
                    continue;
                }
                loopPlayerDetails = false;
            }

            //UI check
            if(inspectCheck) System.out.println("I: Inspect Monster");
            if(attackCheck) System.out.println("E: Attack Monster");
            if(inspectVillageCheck) System.out.println("V: Visit Village");
            if(playerDetailsCheck) System.out.println("Q: Player Details/Stats/Inventory");
            
            if(monsterAttackCheck) loopMonsterAttack = true;
            
            if(player.currentHealth <= 0){
                cls();
                System.out.println("YOU DIED");
                break;
            }
            //Master input for the player
            playerInput = input.nextLine().toUpperCase();
            
            //Player Details
            if(playerInput.equals("Q")){
                loopPlayerDetails = true;
                monsterIsChase = false;
            }
            //Inspect Monster
            if(playerInput.equals("I")){
                loopInspect = true;
                monsterIsChase = false;
            }

            //Attack Monster
            if(playerInput.equals("E")){
                loopPlayerAttack = true;
                monsterAttackCheck = false;
            }
            
            //Inpsect Village
            if(playerInput.equals("V") && !loopVillage && inspectVillageCheck){
                loopVillage = true;
                monsterIsChase = false;
            }
            
            //Player movement mechanic
            switch(playerInput){
                //Forward
                case "W":
                    if (map[player.y-1][player.x].mapRepresentation.equals("M")|| 
                        map[player.y-1][player.x].mapRepresentation.equals("V")) break;
                    player.y-=1; 
                    map[player.y+1][player.x] = notEnt;

                    //Random monster generator 
                    for(int i = player.x-5; i < player.x+6; i++){
                        int randomMonster = rand.nextInt(100);
                        if(randomMonster < 2){
                            Monster monster = new Monster();
                            if(map[player.y-5][i] == monster ||  map[player.y-5][i].mapRepresentation.equals("V")) break;
                            map[player.y-5][i] = monster;
                            monster.y = player.y-5;
                            monster.x = i;
                            monsterData.add(monster);
                        }
                    }
                    break;
                //Left
                case "A": 
                    if (map[player.y][player.x-1].mapRepresentation.equals("M")|| 
                        map[player.y][player.x-1].mapRepresentation.equals("V")) break;
                    player.x-=1; 
                    map[player.y][player.x+1] = notEnt;
                    
                    //Random monster generator 
                    for(int i = player.y-5; i < player.y+6; i++){
                        int randomMonster = rand.nextInt(100);
                        if(randomMonster < 1){
                            Monster monster = new Monster();
                            if(map[i][player.x-5] == monster ||  map[i][player.x-5].mapRepresentation.equals("V")) break;
                            map[i][player.x-5] = monster;
                            monster.y = i;
                            monster.x = player.x-5;
                            monsterData.add(monster);
                        }
                    }
                    break;
                //Down
                case "S": 
                    if (map[player.y+1][player.x].mapRepresentation.equals("M") || 
                        map[player.y+1][player.x].mapRepresentation.equals("V")) break;
                    player.y+=1; 
                    map[player.y-1][player.x] = notEnt;
                    
                    //Random monster generator 
                    for(int i = player.x-5; i < player.x+6; i++){
                        int randomMonster = rand.nextInt(100);
                        if(randomMonster < 1){
                            Monster monster = new Monster();
                            if(map[player.y+5][i] == monster ||  map[player.y+5][i].mapRepresentation.equals("V")) break;
                            map[player.y+5][i] = monster;
                            monster.y = player.y+5;
                            monster.x = i;
                            monsterData.add(monster);
                        }
                    }
                    break;
                //Right
                case "D":
                    if (map[player.y][player.x+1].mapRepresentation.equals("M")|| 
                        map[player.y][player.x+1].mapRepresentation.equals("V")) break; 
                    player.x+=1; 
                    map[player.y][player.x-1] = notEnt; 
                    for(int i = player.y-5; i < player.y+6; i++){
                        int randomMonster = rand.nextInt(100);
                        if(randomMonster < 1){
                            Monster monster = new Monster();
                            if(map[i][player.x+5] == monster ||  map[i][player.x+5].mapRepresentation.equals("V")) break;
                            map[i][player.x+5] = monster;
                            monster.y = i;
                            monster.x = player.x+5;
                            monsterData.add(monster);
                        }
                    }
                    break;
                
                default : break;
            }
            cls();
            //End statement
            if (playerInput.equals("-1")) break;
        }
        input.close();
    }

    //Clear screen method
    public static void cls(){
        try{	
            new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
        }
        catch(Exception E){
                System.out.println(E);
            }
        }
}