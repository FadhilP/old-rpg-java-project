package ProjectA;

import java.util.Random;
/**
 * Monster
 */
public class Monster extends Entity {

    //protected boolean isNamed;
    protected String type;
    protected String monsterDesc;

    public static int gameStage;

    protected static Monster[] monsterArchive = {
        new Monster("Slime", 25, 5, ""),
        new Monster("Goblin", 45, 8, ""),
        new Monster("Hogoblin", 100, 11, ""),
        new Monster("Ogre", 135, 17, ""),
        new Monster("Wolf", 90, 10, ""),
        //new Monster("Bandit", 0, 0, "")
     };


     public Monster(String type, double health, double attackDamage, String description){
        super();
        this.type = type;
        this.maxHealth = health;
        this.currentHealth = maxHealth;
        this.attackDamage = attackDamage;
        this.monsterDesc = description;
    }

    public Monster(){
        super();
        
        int randMonster = new Random().nextInt(monsterArchive.length);
        this.type = monsterArchive[randMonster].type;
        this.maxHealth = monsterArchive[randMonster].maxHealth + rand.nextInt(10);
        this.currentHealth = this.maxHealth;
        this.attackDamage = monsterArchive[randMonster].attackDamage;
        this.monsterDesc = monsterArchive[randMonster].monsterDesc;
        this.mapRepresentation = "M";
        this.currency = 10;

        int counter = 0;
        for(Monster monster : monsterArchive){
            if(monster.type == this.type) this.xp = counter / 2 * 5 + rand.nextInt(counter+2 / 2 * 2);
            counter++;
        }
    }

    public String toString(){
        return String.format("Type : %s\nHealth : %.0f/%.0f\nPosition : {%d, %d}",
        this.type,
        this.currentHealth,
        this.maxHealth,
        this.x,
        this.y);
    }
}