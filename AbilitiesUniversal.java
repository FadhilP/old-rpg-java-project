package ProjectA;

/**
 * AbilitiesUniversal
 */
public interface AbilitiesUniversal {

    public void attack();
    public void dash();
    public void whirlwind();
}